import { makeStyles } from '@material-ui/core/styles';

const styles = makeStyles({

  bigAvatar: {
    margin: 10,
    width: 200,
    height: 200,
    border: 'solid 1px white',
  },
});
export default styles;
