import { makeStyles } from '@material-ui/core/styles';

import colors from '../../assets/styles/colors';

const styles = makeStyles({
  divider: {
    padding: 2,
    width: '110%',
    backgroundColor: colors.backgroundColor,
  },
});


export default styles;
