import { makeStyles } from '@material-ui/core/styles';

const styles = makeStyles({
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    textTransform: 'uppercase',
  },
  pos: {
    marginBottom: 12,
  },


});

export default styles;
