const colors = {
  backgroundColor: '#282c34',
  shadow: 'rgb(0, 0, 0) 0px 5px 20px -10px',
  whiteText: 'white',
};
export default colors;
