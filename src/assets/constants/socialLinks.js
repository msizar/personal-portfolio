import gitIcon from '../images/git.png';
import linkedin from '../images/linkedin.png';
import twitter from '../images/twitter.png';
import bitbucket from '../images/bitbucket.png';

const SOCIAL_LINKS = [
  {
    name: 'linkedin',
    link: 'https://www.linkedin.com/in/themba-msiza-06555a128/',
    icons: linkedin,
  },
  {
    name: 'git',
    link: 'https://github.com/msizar',
    icons: gitIcon,
  },
  {
    name: 'bitbucket',
    link: 'https://bitbucket.org/msizar',
    icons: bitbucket,
  },
  {
    name: 'twitter',
    link: '#',
    icons: twitter,
  },
];

export default SOCIAL_LINKS;
